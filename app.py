from flask import Flask, render_template, request, redirect, url_for
import time
import sys
import os
import cv2
from mutagen.mp4 import MP4

# init app
app = Flask(__name__)

APP_ROOT = os.path.dirname(os.path.abspath(__file__))

# halaman depan
@app.route('/')
def index():
    return render_template('index.html')

# upload video
@app.route('/upload', methods=['POST'])
def upload():
    target = os.path.join(APP_ROOT, 'static/')
    # print(target)

    if not os.path.isdir(target):
        os.mkdir(target)

    for file in request.files.getlist("video"):
        # print(file)
        filename = file.filename
        # print(filename)
        destination = "/".join([target, filename])
        # print(destination)
        file.save(destination)

        # parse object
        predict(filename)

    return redirect(url_for('predict', video=filename))


# halaman predict
@app.route('/predict/<video>', methods=['GET', 'POST'])
def predict(video):
    if request.method == 'POST':
        print("Start Predict")
        return redirect(url_for('result', video=video))
    else:
        # print(video)
        return render_template('predict.html', video=video)


# halaman hasil
@app.route('/hasil/<video>')
def result(video):
    target = os.path.join(APP_ROOT, 'static/')
    f_video = os.path.join(target, video)
    print(f_video)

    # input video
    cap = cv2.VideoCapture(f_video)

    #  get fps
    video_fps = "{0:.2f}".format(cap.get(cv2.CAP_PROP_FPS))

    # get resolution
    v_width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
    v_height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
    res_info = "{}x{}".format(v_width, v_height)

    # get video duration
    v_original = MP4(f_video)
    v_duration = time.strftime('%H:%M:%S', time.gmtime(int(v_original.info.length)))

    # get video filesize
    file = os.stat(f_video)
    file_size = "{0:.2f}".format(file.st_size * 0.000001)

    return render_template('result.html', video=video, fps=video_fps, resolusi=res_info, durasi=v_duration, size=file_size)


# server conf
if __name__ == '__main__':
    app.run()
